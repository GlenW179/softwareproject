import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class DBHandler {
    
    String portnum = "";
    final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    final String DB_URL = "jdbc:mysql://localhost"+portnum+"/musicstore_db";
    final String port1 = ":8084";
    final String port2 = ":80";
    
    final String User = "root";
    final String Pass = "";
    
    User loggedUser;
    
    ArrayList<User> userList;
    ArrayList<Customer> customerList;
    ArrayList<String> productTags;
    ArrayList<Product> productList;
    
    Connection conn;
    
    public boolean connectDB() {
        try {
            Class.forName(JDBC_DRIVER);
            portnum = port1;
            conn = DriverManager.getConnection(DB_URL, User, Pass);
            System.out.println("it connected!");
            return true;
        }
        catch(Exception ex) {
            try {
                Class.forName(JDBC_DRIVER);
                portnum = port2;
                conn = DriverManager.getConnection(DB_URL, User, Pass);
                System.out.println("it connected with port 2!" + conn);
                return true;
            }
            catch(Exception ex2) {
                System.out.println("it didn't connect :(");
                return false;
            }
        }
    }
    
    public void addOrder(int prodid, int custid, int quant, String orderDate) {
        try {
            PreparedStatement query = conn.prepareStatement("INSERT INTO `orders_tbl` VALUES (NULL, "+prodid+", "+custid+", "+quant+", '"+orderDate+"')");
            query.execute();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public ArrayList<Order> getOrders() {
        ArrayList<Order> orderList = new ArrayList<Order>();
        try {
            PreparedStatement query = conn.prepareStatement("SELECT * FROM orders_tbl");
            ResultSet rslt = query.executeQuery();
            while (rslt.next()) {
                orderList.add(new Order(rslt.getInt("orderId"), rslt.getInt("customerId"), rslt.getInt("stockId"), rslt.getInt("purchaseAmt"), rslt.getString("date")));
            }
            System.out.println("orders got!");
            return orderList;
        }
        catch(Exception ex) {
            ex.printStackTrace();
            
        }
        return orderList;
    }

    public Product getSpecificProduct(int id) {
        Product temp = new Product(99, "error", 99, 99, "error", "error", "error");
        try {
            PreparedStatement query = conn.prepareStatement("SELECT * FROM stock_tbl WHERE stockId = "+id);
            ResultSet rslt = query.executeQuery();
            if (rslt.next()) {
                temp = new Product(rslt.getInt("stockId"), rslt.getString("productName"), rslt.getInt("stockAmt"), rslt.getInt("price"), rslt.getString("tag1"), rslt.getString("tag2"), rslt.getString("tag3")); 
            }
            return temp;
        }
        catch(Exception ex) {
            System.out.println("getting product not worked");
            return temp;
        }
    }
    
    public void modifyProduct(int id, int modfield, String boxchoice) {
        String truechoice;
        if (boxchoice.equals("In Stock")) {
            truechoice = "stockAmt";
        } else {
            truechoice = "price";
        }
        try {
            PreparedStatement query = conn.prepareStatement("UPDATE stock_tbl SET "+truechoice+" = "+modfield+" WHERE stock_tbl.stockId = "+id);
            query.execute();
        }
        catch(Exception ex) {
            System.out.println("modifying didn't work");
        }
    }
    
    public void deleteProduct(int id) {
        try {
            PreparedStatement query = conn.prepareStatement("DELETE FROM stock_tbl WHERE stock_tbl.stockId = ?");
            query.setInt(1, id);
            query.execute();
            System.out.println("delete worked!");
        }
        catch (Exception ex) {
            System.out.println("delete didn't work!");
        }
    }
    
    public ArrayList<Product> getProductList(String searchfield, String searchparameter) {
        productList = new ArrayList<Product>();
        String truesearchpara = "";
        if (searchparameter.equals("ID")) {
            truesearchpara = "stockId";
        } else if (searchparameter.equals("Name")) {
            truesearchpara = "productName";
        } else if (searchparameter.equals("In Stock")) {
            truesearchpara = "stockAmt";
        } else {
            truesearchpara= "price";
        }
        try {
            PreparedStatement query = conn.prepareStatement("SELECT * FROM stock_tbl WHERE "+truesearchpara+" = "+searchfield);
            ResultSet rslt = query.executeQuery();
            while (rslt.next()) {
                productList.add(new Product(rslt.getInt("stockId"), rslt.getString("productName"), rslt.getInt("stockAmt"), rslt.getInt("price"), rslt.getString("tag1"), rslt.getString("tag2"), rslt.getString("tag3")));
            }
            System.out.println("product good");
            return productList;
        }
        catch (Exception ex) {
            System.out.println("product list has failed");
            return productList;
        }
    }
    
    public ArrayList<Product> getProductList(String searchfield, String searchparameter, String tag) {
        productList = new ArrayList<Product>();
        String truesearchpara = "";
        if (searchparameter.equals("ID")) {
            truesearchpara = "stockId";
        } else if (searchparameter.equals("Name")) {
            truesearchpara = "productName";
        } else if (searchparameter.equals("In Stock")) {
            truesearchpara = "stockAmt";
        } else {
            truesearchpara= "price";
        }
        try {
            PreparedStatement query = conn.prepareStatement("SELECT * FROM stock_tbl WHERE "+truesearchpara+" = "+searchfield);
            ResultSet rslt = query.executeQuery();
            while (rslt.next()) {
                if (rslt.getString("tag1").equals(tag.toLowerCase()) || rslt.getString("tag2").equals(tag.toLowerCase()) || rslt.getString("tag3").equals(tag.toLowerCase())) {
                    System.out.println(tag.toLowerCase());
                    productList.add(new Product(rslt.getInt("stockId"), rslt.getString("productName"), rslt.getInt("stockAmt"), rslt.getInt("price"), rslt.getString("tag1"), rslt.getString("tag2"), rslt.getString("tag3")));
                }
                
            }
            System.out.println("product list (no tags) done!");
            return productList;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("product list (no tags) has failed");
            return productList;
        }
    }
    
    public ArrayList<Product> getAllProducts() {
        productList = new ArrayList<Product>();
        try {
            PreparedStatement query = conn.prepareStatement("SELECT * FROM stock_tbl");
            ResultSet rslt = query.executeQuery();
            
            while (rslt.next()) {
                productList.add(new Product(rslt.getInt("stockId"), rslt.getString("productName"), rslt.getInt("stockAmt"), rslt.getInt("price"), rslt.getString("tag1"), rslt.getString("tag2"), rslt.getString("tag3")));
            }
            
            return productList;
        }
        catch(Exception ex) {
            return productList;
        }
    }
    
    public ArrayList<String> getProductTags() {
        productTags = new ArrayList<String>();
        try {
            PreparedStatement query = conn.prepareStatement("SELECT tag1, tag2, tag3 FROM stock_tbl");
            ResultSet rslt = query.executeQuery();
            while (rslt.next()) {
                if (!productTags.contains(rslt.getString("tag1"))) {
                    productTags.add(rslt.getString("tag1"));
                }
                if (!productTags.contains(rslt.getString("tag2"))) {
                    productTags.add(rslt.getString("tag2"));
                }
                if (!productTags.contains(rslt.getString("tag3"))) {
                    productTags.add(rslt.getString("tag3"));
                }
            }
            System.out.println("tags worked!");
            return productTags;
        } catch (Exception ex) {
            ex.printStackTrace();
            return productTags;
        }
    }
    
    public ArrayList<Customer> getCustomerList() {
        return customerList;
    }
    
    public ArrayList<User> getUserList() {
        return userList;
    }
    
    public void populateCustomerList() {
        customerList = new ArrayList<Customer>();
        try {
            PreparedStatement query = conn.prepareStatement("Select * From customers_tbl");
            ResultSet rslt = query.executeQuery();
            System.out.println("customer list population retrieved!");
            while(rslt.next()) {
                customerList.add(new Customer(rslt.getInt("customerId"), rslt.getString("name"), rslt.getString("surname"), rslt.getString("email")));
            }
        }
        catch(Exception ex) {
            System.out.println("customer list population failed!");
        }
    }
    
    public void populateUserList() {
        userList = new ArrayList<User>();
        try {
            PreparedStatement query = conn.prepareStatement("Select * From users_tbl");
            ResultSet rslt = query.executeQuery();
            System.out.println("user list population retrieved!");
            while(rslt.next()) {
                userList.add(new User(rslt.getInt("userId"), rslt.getString("name"), rslt.getString("surname"), rslt.getString("username"), rslt.getString("password"), rslt.getBoolean("isAdmin")));
            }
        }
        catch(Exception ex) {
            System.out.println("user list population failed!");
        }
    }
    
    public int usrLogin(String username, String password) {
        String dbPass = "";
        String dbName = "";
        String dbSurname = "";
        boolean isAdmin = false;
        try {
            PreparedStatement query = conn.prepareStatement("SELECT * FROM users_tbl WHERE users_tbl.username = \""+username+"\"");
            ResultSet rslt = query.executeQuery();
            System.out.println("query worked!");
            while (rslt.next()) {
                dbPass = rslt.getString("password");
                System.out.println(dbPass);
                System.out.println(password);
                dbName = rslt.getString("name");
                dbSurname = rslt.getString("surname");
                isAdmin = rslt.getBoolean("isAdmin");
                System.out.println(isAdmin);
            }
            if (dbPass.equals(password)) {
                loggedUser = new User(dbName, dbSurname, isAdmin);
                if (isAdmin) {
                    return 2;
                } else {
                    return 1;
                }
            } else {
                System.out.println("passwords don't match!");
                return 0;
            }
            
            
        } catch (Exception ex) {
            System.out.println("something went wrong with logging in!");
            return 0;
        }
    }
    
    public User getLoggedUsr() {
        return loggedUser;
    }
    
    public void deleteCustomer(int id) {
        try {
            PreparedStatement query = conn.prepareStatement("DELETE FROM customers_tbl WHERE customers_tbl.customerId = ?");
            query.setInt(1, id);
            query.execute();
            System.out.println("delete worked!");
        }
        catch (Exception ex) {
            System.out.println("delete didn't work!");
        }
    }
    
    public void deleteUsr(int id) {
        try {
            PreparedStatement query = conn.prepareStatement("DELETE FROM users_tbl WHERE users_tbl.userId = ?");
            query.setInt(1, id);
            query.execute();
            System.out.println("delete worked!");
        }
        catch (Exception ex) {
            System.out.println("delete didn't work!");
        }
    }
    
    public void addUsr(String nm, String srnm, String usrnm, String psswrd, boolean admin) {
        int isadmin;
        if (admin) {
            isadmin = 1;
        } else {
            isadmin = 0;
        }
        try {
            PreparedStatement query = conn.prepareStatement("INSERT INTO `users_tbl` (`userId`, `name`, `surname`, `username`, `password`, `isAdmin`) VALUES (NULL, '"+nm+"', '"+srnm+"', '"+usrnm+"', '"+psswrd+"', '"+isadmin+"')");
            query.execute();
        }
        catch (Exception ex) {
            ex.printStackTrace(); 
            System.out.println("add didn't work");
        }
    }
    
    public void addCustomer(String nm, String srnm, String eml) {
        try {
            PreparedStatement query = conn.prepareStatement("INSERT INTO `customers_tbl` (`customerId`, `name`, `surname`, `email`) VALUES (NULL, '"+nm+"', '"+srnm+"', '"+eml+"')");
            query.execute();
        }
        catch (Exception ex) {
            ex.printStackTrace(); 
            System.out.println("add didn't work");
        }
    }
    
    public void addStock(String name, int stock, int price, String tag1, String tag2, String tag3) {
        try {
            PreparedStatement query = conn.prepareStatement("INSERT INTO `stock_tbl` (`stockId`, `productName`, `stockAmt`, `price`, `tag1`, `tag2`, `tag3`) "
                    + "VALUES (NULL, ?, ?, ?, ?, ?, ?)");
            query.setString(1, name);
            query.setInt(2, stock);
            query.setInt(3, price);
            query.setString(4, tag1);
            query.setString(5, tag2);
            query.setString(6, tag3);
            query.execute();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("no stock added");
        }
    }
}
