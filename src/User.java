public class User {
    private String name, surname, username, password;
    int id;
    boolean isAdmin;
    public User (String name, String surname, boolean isAdmin) {
        this.name = name;
        this.surname = surname;
        this.isAdmin = isAdmin;
    }
    
    public User(int id, String name, String surname, String username, String password, boolean isAdmin) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.isAdmin = isAdmin;
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public String getSurname() {
        return surname;
    }
 
    public boolean getIsAdmin() {
        return isAdmin;
    }
    
    public String getPassword() {
        return password;
    }
    
    public String getUsername() {
        return username;
    }
    
    public int getId() {
        return id;
    }
}
