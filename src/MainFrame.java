
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.function.Predicate;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Eman
 */
public class MainFrame extends javax.swing.JFrame {
    
    public User loggedUser;
    public DBHandler dbh = new DBHandler();
    boolean connected = dbh.connectDB();
    ArrayList<Product> cart = new ArrayList<Product>();
    
    
    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        initComponents();
        Tabs.setVisible(false);
        LoggedUser.setVisible(false);
        ReceiptPane.setFont(ReceiptPane.getFont().deriveFont(20f));
        initiateStock();
    }
    
    public void initiateStock() {
        ArrayList<String> ptags = dbh.getProductTags();
        for (int i = 0; i < ptags.size(); i++) {
            String temp = ptags.get(i).substring(0, 1).toUpperCase() + ptags.get(i).substring(1);
            TagChoice.addItem(temp);
        }
    }
    
    public void createReceipt() {
        String receiptText = "Receipt:\n";
        float total = 0;
        for(int i = 0; i < cart.size(); i++) {
            receiptText += "$"+cart.get(i).getPrice() + " "+cart.get(i).getProductName()+" x("+cart.get(i).getStockAmt()+")\n";
            total = total + cart.get(i).getPrice();
        }
        receiptText += "------------------\n$"+total+" Total\nCustomer ID: "+SCCustomerID.getText();
        ReceiptPane.setText(receiptText);
        
        String cartDate = jSpinner6.getValue().toString()+"-"+jSpinner5.getValue().toString()+"-"+jSpinner4.getValue().toString();        
        System.out.println(cartDate);
        dbh.addOrder(Integer.parseInt(SCCustomerID.getText()), Integer.parseInt(ProductIDField.getText()), Integer.parseInt(ProductQuantityField.getText()), cartDate);
        
    }
    
    public void updateOrders() {
        ArrayList<Order> orderList = dbh.getOrders();
        DefaultTableModel model = (DefaultTableModel) jTable5.getModel();
        while(model.getRowCount() > 0)
        {
            model.removeRow(0);
        }
        for (int i = 0; i < orderList.size(); i++) {
            model.addRow(new Object[]{orderList.get(i).orderid, orderList.get(i).customerid, orderList.get(i).stockid, orderList.get(i).purchaseAmt, orderList.get(i).date});
            System.out.println(orderList.get(i).date);
        }
    }
    
    public void updateShoppingCart() {
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        while(model.getRowCount() > 0)
        {
            model.removeRow(0);
        }
        for (int i = 0; i < cart.size(); i++) {
            model.addRow(new Object[]{cart.get(i).getId(), cart.get(i).getProductName(), cart.get(i).getStockAmt(), cart.get(i).getPrice()});
        }
            
    }
    
    public void updateStockTable() {
        ArrayList<Product> prodlist = new ArrayList<Product>();
        String tagChoice = (String)TagChoice.getSelectedItem();
        String paraChoice = (String)SearchParameter.getSelectedItem();
        DefaultTableModel model = (DefaultTableModel) jTable3.getModel();
        
        prodlist = dbh.getAllProducts();
        
        while(model.getRowCount() > 0) {
            model.removeRow(0);
        }
        for(int i = 0; i < prodlist.size(); i++) {
            Product temp = prodlist.get(i);
            model.addRow(new Object[]{temp.getId(), temp.getProductName(), temp.getStockAmt(), temp.getPrice(), temp.getTag1(), temp.getTag2(), temp.getTag3()});
        }
    }
    
    public void stockSearchResults() {
        ArrayList<Product> prodlist = new ArrayList<Product>();
        String tagChoice = (String)TagChoice.getSelectedItem();
        String paraChoice = (String)SearchParameter.getSelectedItem();
        DefaultTableModel model = (DefaultTableModel) jTable3.getModel();
        
        if (!tagChoice.equals("None")) {
            prodlist = dbh.getProductList(SearchField.getText(), paraChoice, tagChoice);
            System.out.println("a tag!");
        } else {
            prodlist = dbh.getProductList(SearchField.getText(), paraChoice);
            System.out.println("no tag!");
        }
        
        while(model.getRowCount() > 0) {
            model.removeRow(0);
        }
        for(int i = 0; i < prodlist.size(); i++) {
            Product temp = prodlist.get(i);
            model.addRow(new Object[]{temp.getId(), temp.getProductName(), temp.getStockAmt(), temp.getPrice(), temp.getTag1(), temp.getTag2(), temp.getTag3()});
        }
    }
    
    public void updateCustomerTable() {
        dbh.populateCustomerList();
        ArrayList<Customer> customerList = dbh.getCustomerList();
        DefaultTableModel model = (DefaultTableModel) CustomerResultTable.getModel();
        while(model.getRowCount() > 0) {
            model.removeRow(0);
        }
        for(int i = 0; i < customerList.size(); i++) {
            Customer temp = customerList.get(i);
            model.addRow(new Object[]{temp.getId(), temp.getName(), temp.getSurname(), temp.getEmail()});
        }
    }
    
    public void updateUserTable() {
        dbh.populateUserList();
        ArrayList<User> userList = dbh.getUserList();
        DefaultTableModel model = (DefaultTableModel) UserResultTable.getModel();
        while(model.getRowCount() > 0)
        {
            model.removeRow(0);
        }
        for(int i = 0; i < userList.size(); i++) {
            User temp = userList.get(i);
            model.addRow(new Object[]{temp.getId(), temp.getName(), temp.getSurname(), temp.getUsername(), temp.getPassword(), temp.getIsAdmin()});
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BottomPane = new javax.swing.JPanel();
        LogText = new javax.swing.JLabel();
        Username = new javax.swing.JTextField();
        LoginButton = new javax.swing.JButton();
        LoggedUser = new javax.swing.JLabel();
        Password = new javax.swing.JPasswordField();
        Tabs = new javax.swing.JTabbedPane();
        Sales = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable5 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        SalesSearchButton = new javax.swing.JButton();
        SalesIDPast = new javax.swing.JTextField();
        jSpinner1 = new javax.swing.JSpinner();
        jSpinner2 = new javax.swing.JSpinner();
        jSpinner3 = new javax.swing.JSpinner();
        Stock = new javax.swing.JPanel();
        SearchField = new javax.swing.JTextField();
        SearchParameter = new javax.swing.JComboBox<>();
        SearchText = new javax.swing.JLabel();
        SearchButtonStock = new javax.swing.JButton();
        TagText = new javax.swing.JLabel();
        TagChoice = new javax.swing.JComboBox<>();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jSeparator1 = new javax.swing.JSeparator();
        StockAdminPanel = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        SDProdID = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        SAName = new javax.swing.JTextField();
        SAStock = new javax.swing.JTextField();
        SAPrice = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        SATag2 = new javax.swing.JTextField();
        SATag1 = new javax.swing.JTextField();
        SATag3 = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        ShowAllStockButton = new javax.swing.JButton();
        ModField = new javax.swing.JTextField();
        CustomerAccounts = new javax.swing.JPanel();
        CustomerResultScroll = new javax.swing.JScrollPane();
        CustomerResultTable = new javax.swing.JTable();
        CRegisterText1 = new javax.swing.JLabel();
        CNameText1 = new javax.swing.JLabel();
        CName1 = new javax.swing.JTextField();
        CSurnameText1 = new javax.swing.JLabel();
        CSurname1 = new javax.swing.JTextField();
        CUsernameText1 = new javax.swing.JLabel();
        CEmail = new javax.swing.JTextField();
        CAddButton1 = new javax.swing.JButton();
        CRemoveText1 = new javax.swing.JLabel();
        CUserIDText1 = new javax.swing.JTextField();
        CUserIdText1 = new javax.swing.JLabel();
        CRemoveButton1 = new javax.swing.JButton();
        UserAccounts = new javax.swing.JPanel();
        UAName = new javax.swing.JTextField();
        UASurname = new javax.swing.JTextField();
        UAUsername = new javax.swing.JTextField();
        UAPassword = new javax.swing.JTextField();
        AdminCheckBox = new javax.swing.JCheckBox();
        UARegisterText = new javax.swing.JLabel();
        UANameText = new javax.swing.JLabel();
        UASurnameText = new javax.swing.JLabel();
        UAUsernameText = new javax.swing.JLabel();
        UAPasswordText = new javax.swing.JLabel();
        UAAddButton = new javax.swing.JButton();
        UARemoveText = new javax.swing.JLabel();
        UAUserIDLabel = new javax.swing.JLabel();
        UAUserIDText = new javax.swing.JTextField();
        UARemoveButton = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        UserResultTable = new javax.swing.JTable();
        ShoppingCart = new javax.swing.JPanel();
        SCLabel = new javax.swing.JLabel();
        ProductIDField = new javax.swing.JTextField();
        SCRemoveButton = new javax.swing.JButton();
        SCAddButton = new javax.swing.JButton();
        SCGenButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        ReceiptPane = new javax.swing.JTextArea();
        jLabel16 = new javax.swing.JLabel();
        ProductQuantityField = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        SCCustomerID = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jSpinner4 = new javax.swing.JSpinner();
        jSpinner5 = new javax.swing.JSpinner();
        jSpinner6 = new javax.swing.JSpinner();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        BottomPane.setBorder(new javax.swing.border.MatteBorder(null));

        LogText.setText("Please log in:");

        Username.setText("Username");
        Username.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UsernameActionPerformed(evt);
            }
        });

        LoginButton.setText("Log In");
        LoginButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LoginButtonMouseClicked(evt);
            }
        });

        LoggedUser.setText("jLabel1");

        javax.swing.GroupLayout BottomPaneLayout = new javax.swing.GroupLayout(BottomPane);
        BottomPane.setLayout(BottomPaneLayout);
        BottomPaneLayout.setHorizontalGroup(
            BottomPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BottomPaneLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(BottomPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LogText)
                    .addGroup(BottomPaneLayout.createSequentialGroup()
                        .addComponent(Username, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Password, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(LoginButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(LoggedUser)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        BottomPaneLayout.setVerticalGroup(
            BottomPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, BottomPaneLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(LogText)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(BottomPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LoginButton)
                    .addComponent(Username, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LoggedUser)
                    .addComponent(Password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(30, Short.MAX_VALUE))
        );

        jTable5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Order ID", "Customer ID", "Product", "Amount", "Date"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable5);

        jLabel2.setText("Search Past Sales");

        jLabel4.setText("Date (dd/mm/yy)");

        jLabel5.setText("Customer ID");

        SalesSearchButton.setText("Search");
        SalesSearchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalesSearchButtonActionPerformed(evt);
            }
        });

        SalesIDPast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalesIDPastActionPerformed(evt);
            }
        });

        jSpinner1.setModel(new javax.swing.SpinnerNumberModel(1, 1, 31, 1));

        jSpinner2.setModel(new javax.swing.SpinnerNumberModel(1, 1, 12, 1));

        jSpinner3.setModel(new javax.swing.SpinnerNumberModel(1900, 1900, 2200, 1));

        javax.swing.GroupLayout SalesLayout = new javax.swing.GroupLayout(Sales);
        Sales.setLayout(SalesLayout);
        SalesLayout.setHorizontalGroup(
            SalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SalesLayout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addComponent(jLabel1)
                .addGap(194, 194, 194)
                .addGroup(SalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addGroup(SalesLayout.createSequentialGroup()
                        .addGap(70, 70, 70)
                        .addGroup(SalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addGroup(SalesLayout.createSequentialGroup()
                                .addGap(54, 54, 54)
                                .addGroup(SalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(SalesIDPast, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(SalesLayout.createSequentialGroup()
                                        .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jSpinner2, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jSpinner3, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(SalesLayout.createSequentialGroup()
                        .addGap(86, 86, 86)
                        .addComponent(SalesSearchButton)))
                .addGap(245, 245, 245))
            .addGroup(SalesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        SalesLayout.setVerticalGroup(
            SalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SalesLayout.createSequentialGroup()
                .addGroup(SalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(SalesLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jLabel1)
                        .addGap(244, 244, 244))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SalesLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel2)
                        .addGap(14, 14, 14)
                        .addGroup(SalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jSpinner2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jSpinner3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(SalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(SalesIDPast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(102, 102, 102)
                        .addComponent(SalesSearchButton)
                        .addGap(18, 18, 18)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE))
        );

        Tabs.addTab("Orders", Sales);

        SearchParameter.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ID", "Name", "In Stock", "Price" }));

        SearchText.setText("Enter search term and choose parameter:");

        SearchButtonStock.setText("Search");
        SearchButtonStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SearchButtonStockActionPerformed(evt);
            }
        });

        TagText.setText("Filter by tag:");

        TagChoice.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "None" }));

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID", "Name", "In Stock", "Price"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(jTable3);

        jLabel7.setText("Product ID");

        SDProdID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SDProdIDActionPerformed(evt);
            }
        });

        jButton1.setText("Remove");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "In Stock", "Price" }));

        jLabel9.setText("Field");

        jButton2.setText("Modify");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel10.setText("Name");

        jLabel11.setText("Stock");

        jLabel12.setText("Price");

        jLabel13.setText("Tag 1");

        jLabel14.setText("Tag 2");

        jLabel15.setText("Tag 3");

        jButton3.setText("Add");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        ShowAllStockButton.setText("Show All");
        ShowAllStockButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ShowAllStockButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout StockAdminPanelLayout = new javax.swing.GroupLayout(StockAdminPanel);
        StockAdminPanel.setLayout(StockAdminPanelLayout);
        StockAdminPanelLayout.setHorizontalGroup(
            StockAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, StockAdminPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(StockAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(StockAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2)
                    .addGroup(StockAdminPanelLayout.createSequentialGroup()
                        .addComponent(SDProdID, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1))
                    .addGroup(StockAdminPanelLayout.createSequentialGroup()
                        .addComponent(ModField, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 137, Short.MAX_VALUE)
                .addGroup(StockAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12))
                .addGap(32, 32, 32)
                .addGroup(StockAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(SAPrice)
                    .addComponent(SAStock)
                    .addComponent(SAName, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(StockAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(StockAdminPanelLayout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(SATag1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(StockAdminPanelLayout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(SATag2, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(StockAdminPanelLayout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(StockAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton3)
                            .addComponent(SATag3, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(57, 57, 57))
            .addGroup(StockAdminPanelLayout.createSequentialGroup()
                .addGap(340, 340, 340)
                .addComponent(ShowAllStockButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        StockAdminPanelLayout.setVerticalGroup(
            StockAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(StockAdminPanelLayout.createSequentialGroup()
                .addGroup(StockAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(StockAdminPanelLayout.createSequentialGroup()
                        .addGroup(StockAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(StockAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel10)
                                .addComponent(SAName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(StockAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel7)
                                .addComponent(SDProdID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jButton1)
                                .addComponent(jLabel13)
                                .addComponent(SATag1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(StockAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9)
                            .addComponent(jLabel14)
                            .addComponent(SATag2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ModField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(6, 6, 6)
                        .addGroup(StockAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(SATag3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton2)))
                    .addGroup(StockAdminPanelLayout.createSequentialGroup()
                        .addGroup(StockAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(SAStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(StockAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(SAPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(4, 4, 4)
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 76, Short.MAX_VALUE)
                .addComponent(ShowAllStockButton)
                .addContainerGap())
        );

        javax.swing.GroupLayout StockLayout = new javax.swing.GroupLayout(Stock);
        Stock.setLayout(StockLayout);
        StockLayout.setHorizontalGroup(
            StockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, StockLayout.createSequentialGroup()
                .addGroup(StockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(StockAdminPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(StockLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(StockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jSeparator1)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, StockLayout.createSequentialGroup()
                                .addGroup(StockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(SearchText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(StockLayout.createSequentialGroup()
                                        .addComponent(TagText)
                                        .addGap(0, 0, Short.MAX_VALUE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(StockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(TagChoice, 0, 179, Short.MAX_VALUE)
                                    .addComponent(SearchField))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(StockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(SearchParameter, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(SearchButtonStock, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap())
        );
        StockLayout.setVerticalGroup(
            StockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, StockLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(StockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SearchParameter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SearchField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SearchText))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(StockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SearchButtonStock, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TagChoice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TagText))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(StockAdminPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        Tabs.addTab("Stock", Stock);

        CustomerResultTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID", "Name", "Surname", "Email"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        CustomerResultScroll.setViewportView(CustomerResultTable);

        CRegisterText1.setText("Register New Customer");

        CNameText1.setText("Name");

        CSurnameText1.setText("Surname");

        CUsernameText1.setText("Email");

        CAddButton1.setText("Add Customer");
        CAddButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CAddButton1ActionPerformed(evt);
            }
        });

        CRemoveText1.setText("Remove Customer");

        CUserIdText1.setText("Customer ID");

        CRemoveButton1.setText("Remove");
        CRemoveButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CRemoveButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout CustomerAccountsLayout = new javax.swing.GroupLayout(CustomerAccounts);
        CustomerAccounts.setLayout(CustomerAccountsLayout);
        CustomerAccountsLayout.setHorizontalGroup(
            CustomerAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CustomerAccountsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CustomerResultScroll)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CustomerAccountsLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(CustomerAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(CustomerAccountsLayout.createSequentialGroup()
                        .addGroup(CustomerAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CustomerAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CustomerAccountsLayout.createSequentialGroup()
                                    .addComponent(CNameText1)
                                    .addGap(80, 80, 80))
                                .addGroup(CustomerAccountsLayout.createSequentialGroup()
                                    .addComponent(CSurnameText1)
                                    .addGap(61, 61, 61)))
                            .addGroup(CustomerAccountsLayout.createSequentialGroup()
                                .addComponent(CUsernameText1)
                                .addGap(54, 54, 54)))
                        .addGroup(CustomerAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(CEmail)
                            .addComponent(CSurname1)
                            .addComponent(CName1)
                            .addComponent(CAddButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(CustomerAccountsLayout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addComponent(CRegisterText1)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 271, Short.MAX_VALUE)
                .addGroup(CustomerAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CustomerAccountsLayout.createSequentialGroup()
                        .addComponent(CRemoveText1)
                        .addGap(117, 117, 117))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CustomerAccountsLayout.createSequentialGroup()
                        .addComponent(CUserIdText1)
                        .addGap(32, 32, 32)
                        .addComponent(CUserIDText1, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CustomerAccountsLayout.createSequentialGroup()
                        .addComponent(CRemoveButton1)
                        .addGap(129, 129, 129))))
        );
        CustomerAccountsLayout.setVerticalGroup(
            CustomerAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CustomerAccountsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(CustomerAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(CustomerAccountsLayout.createSequentialGroup()
                        .addGroup(CustomerAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(CRegisterText1)
                            .addGroup(CustomerAccountsLayout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addGroup(CustomerAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(CNameText1)
                                    .addComponent(CName1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(CustomerAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(CSurnameText1)
                                    .addComponent(CSurname1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(CustomerAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(CUsernameText1)
                                    .addComponent(CEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(36, 36, 36)
                        .addGroup(CustomerAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(CAddButton1)
                            .addComponent(CRemoveButton1)))
                    .addGroup(CustomerAccountsLayout.createSequentialGroup()
                        .addComponent(CRemoveText1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(CustomerAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(CUserIDText1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CUserIdText1))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CustomerResultScroll, javax.swing.GroupLayout.DEFAULT_SIZE, 330, Short.MAX_VALUE))
        );

        Tabs.addTab("Customers", CustomerAccounts);

        AdminCheckBox.setText("Admin");

        UARegisterText.setText("Register New User");

        UANameText.setText("Name");

        UASurnameText.setText("Surname");

        UAUsernameText.setText("Username");

        UAPasswordText.setText("Password");

        UAAddButton.setText("Add User");
        UAAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UAAddButtonActionPerformed(evt);
            }
        });

        UARemoveText.setText("Remove User");

        UAUserIDLabel.setText("User ID");

        UARemoveButton.setText("Remove");
        UARemoveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UARemoveButtonActionPerformed(evt);
            }
        });

        UserResultTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "ID", "Name", "Surname", "Username", "Password", "Admin"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, true, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(UserResultTable);

        javax.swing.GroupLayout UserAccountsLayout = new javax.swing.GroupLayout(UserAccounts);
        UserAccounts.setLayout(UserAccountsLayout);
        UserAccountsLayout.setHorizontalGroup(
            UserAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, UserAccountsLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(UserAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, UserAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, UserAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, UserAccountsLayout.createSequentialGroup()
                                .addComponent(UANameText)
                                .addGap(80, 80, 80))
                            .addGroup(UserAccountsLayout.createSequentialGroup()
                                .addComponent(UASurnameText)
                                .addGap(61, 61, 61)))
                        .addGroup(UserAccountsLayout.createSequentialGroup()
                            .addComponent(UAUsernameText)
                            .addGap(54, 54, 54)))
                    .addGroup(UserAccountsLayout.createSequentialGroup()
                        .addGroup(UserAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(AdminCheckBox)
                            .addComponent(UAPasswordText))
                        .addGap(49, 49, 49)))
                .addGroup(UserAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(UAUsername)
                    .addComponent(UAPassword)
                    .addComponent(UASurname)
                    .addComponent(UAName)
                    .addComponent(UAAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(UserAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, UserAccountsLayout.createSequentialGroup()
                        .addComponent(UAUserIDLabel)
                        .addGap(32, 32, 32)
                        .addComponent(UAUserIDText, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(88, 88, 88))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, UserAccountsLayout.createSequentialGroup()
                        .addComponent(UARemoveButton)
                        .addGap(109, 109, 109))))
            .addGroup(UserAccountsLayout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addComponent(UARegisterText)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(UARemoveText)
                .addGap(125, 125, 125))
            .addGroup(UserAccountsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
        );
        UserAccountsLayout.setVerticalGroup(
            UserAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, UserAccountsLayout.createSequentialGroup()
                .addGroup(UserAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(UserAccountsLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(UserAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(UARegisterText)
                            .addComponent(UARemoveText))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(UserAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(UAUserIDText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(UAUserIDLabel)))
                    .addGroup(UserAccountsLayout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(UserAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(UANameText)
                            .addComponent(UAName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(UserAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(UASurnameText)
                            .addComponent(UASurname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(UserAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(UAUsernameText)
                            .addComponent(UAPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(UserAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(UAUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(UAPasswordText))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(UserAccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(UAAddButton)
                    .addComponent(AdminCheckBox)
                    .addComponent(UARemoveButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 330, Short.MAX_VALUE))
        );

        Tabs.addTab("Staff Accounts", UserAccounts);

        SCLabel.setText("Product ID");

        SCRemoveButton.setText("Remove");
        SCRemoveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SCRemoveButtonActionPerformed(evt);
            }
        });

        SCAddButton.setText("Add");
        SCAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SCAddButtonActionPerformed(evt);
            }
        });

        SCGenButton.setText("Generate Receipt");
        SCGenButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SCGenButtonActionPerformed(evt);
            }
        });

        jTable1.setAutoCreateRowSorter(true);
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Id", "Product", "Quantity", "Price"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setFocusable(false);
        jScrollPane2.setViewportView(jTable1);

        ReceiptPane.setEditable(false);
        ReceiptPane.setColumns(20);
        ReceiptPane.setLineWrap(true);
        ReceiptPane.setRows(5);
        jScrollPane5.setViewportView(ReceiptPane);

        jLabel16.setText("Quantity");

        jLabel8.setText("Customer ID");

        jLabel17.setText("Date (dd/mm/yy)");

        jSpinner4.setModel(new javax.swing.SpinnerNumberModel(0, 0, 31, 1));

        jSpinner5.setModel(new javax.swing.SpinnerNumberModel(0, 0, 12, 1));

        jSpinner6.setModel(new javax.swing.SpinnerNumberModel(1900, 1900, 2100, 1));

        javax.swing.GroupLayout ShoppingCartLayout = new javax.swing.GroupLayout(ShoppingCart);
        ShoppingCart.setLayout(ShoppingCartLayout);
        ShoppingCartLayout.setHorizontalGroup(
            ShoppingCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ShoppingCartLayout.createSequentialGroup()
                .addGroup(ShoppingCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ShoppingCartLayout.createSequentialGroup()
                        .addGroup(ShoppingCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(ShoppingCartLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jScrollPane5))
                            .addGroup(ShoppingCartLayout.createSequentialGroup()
                                .addGroup(ShoppingCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(ShoppingCartLayout.createSequentialGroup()
                                        .addGap(24, 24, 24)
                                        .addGroup(ShoppingCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel16)
                                            .addComponent(jLabel8)
                                            .addComponent(SCLabel))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(ShoppingCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(ProductQuantityField)
                                            .addComponent(ProductIDField)
                                            .addComponent(SCCustomerID, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)))
                                    .addGroup(ShoppingCartLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(SCAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(SCRemoveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(ShoppingCartLayout.createSequentialGroup()
                                        .addGap(84, 84, 84)
                                        .addComponent(SCGenButton)))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(ShoppingCartLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(ShoppingCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel17)
                            .addGroup(ShoppingCartLayout.createSequentialGroup()
                                .addGap(124, 124, 124)
                                .addComponent(jSpinner4, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jSpinner5, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jSpinner6, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)))
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 467, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        ShoppingCartLayout.setVerticalGroup(
            ShoppingCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ShoppingCartLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ShoppingCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ShoppingCartLayout.createSequentialGroup()
                        .addGroup(ShoppingCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ProductIDField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(SCLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(ShoppingCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ProductQuantityField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(ShoppingCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(SCCustomerID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(ShoppingCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(SCAddButton)
                            .addComponent(SCRemoveButton))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(ShoppingCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17)
                            .addComponent(jSpinner4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jSpinner5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jSpinner6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(50, 50, 50)
                        .addComponent(SCGenButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );

        Tabs.addTab("Shopping Cart", ShoppingCart);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Tabs)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(BottomPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(Tabs)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BottomPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void UsernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UsernameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_UsernameActionPerformed

    private void LoginButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LoginButtonMouseClicked
        try {
            int success = dbh.usrLogin(Username.getText().trim(), Password.getText().trim());
            if(success == 1) {
                System.out.println("user logged, not admin!");
                loggedUser = dbh.getLoggedUsr();
                LoggedUser.setText(loggedUser.getName()+" "+loggedUser.getSurname());
                Tabs.remove(UserAccounts);
            } else if (success == 2) {
                System.out.println("user logged");
                loggedUser = dbh.getLoggedUsr();
                LoggedUser.setText(loggedUser.getName()+" "+loggedUser.getSurname()+" as Admin");
            } else {
                System.out.println("NO!!!!");
            }

            if (success == 1 || success == 2) {
                Username.setVisible(false);
                Password.setVisible(false);
                LoginButton.setVisible(false);
                LoggedUser.setVisible(true);
                LogText.setText("Currently logged in:");
                //Tabs.remove(Receipt);
                Tabs.setVisible(true);
                updateUserTable();
                updateCustomerTable();
            }
        } catch(Exception ex) {
            JOptionPane.showMessageDialog(null, "Input Error");
        }
    }//GEN-LAST:event_LoginButtonMouseClicked

    private void SalesSearchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalesSearchButtonActionPerformed
        updateOrders();
    }//GEN-LAST:event_SalesSearchButtonActionPerformed

    private void SalesIDPastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalesIDPastActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_SalesIDPastActionPerformed

    private void UARemoveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UARemoveButtonActionPerformed
        try {
            dbh.deleteUsr(Integer.parseInt(UAUserIDText.getText()));
            updateUserTable();
        } catch(Exception ex) {
            JOptionPane.showMessageDialog(null, "Input Error");
        }
    }//GEN-LAST:event_UARemoveButtonActionPerformed

    private void UAAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UAAddButtonActionPerformed
        try {
            dbh.addUsr(UAName.getText(), UASurname.getText(), UAUsername.getText(), UAPassword.getText(), AdminCheckBox.isSelected());
            updateUserTable();
        } catch(Exception ex) {
            JOptionPane.showMessageDialog(null, "Input Error");
        }
    }//GEN-LAST:event_UAAddButtonActionPerformed

    private void CAddButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CAddButton1ActionPerformed
        try {
            dbh.addCustomer(CName1.getText(), CSurname1.getText(), CEmail.getText());
            updateCustomerTable();
        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Input Error");
        }
    }//GEN-LAST:event_CAddButton1ActionPerformed

    private void CRemoveButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CRemoveButton1ActionPerformed

        if(!CUserIDText1.getText().isEmpty()) {
            try{
                dbh.deleteCustomer(Integer.parseInt(CUserIDText1.getText()));
                updateCustomerTable();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Input Error");
            }
        }
    }//GEN-LAST:event_CRemoveButton1ActionPerformed

    private void SDProdIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SDProdIDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_SDProdIDActionPerformed

    private void SCGenButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SCGenButtonActionPerformed
        createReceipt();
    }//GEN-LAST:event_SCGenButtonActionPerformed

    private void SCAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SCAddButtonActionPerformed
        Product oneproduct = dbh.getSpecificProduct(Integer.parseInt(ProductIDField.getText()));
        if (!oneproduct.getProductName().equals("error")) {
            oneproduct.price *= Float.parseFloat(ProductQuantityField.getText());
            oneproduct.stockAmt = Integer.parseInt(ProductQuantityField.getText());
            cart.add(oneproduct);
            updateShoppingCart(); 
        }  
    }//GEN-LAST:event_SCAddButtonActionPerformed

    private void SearchButtonStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SearchButtonStockActionPerformed
        stockSearchResults();
    }//GEN-LAST:event_SearchButtonStockActionPerformed

    private void ShowAllStockButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ShowAllStockButtonActionPerformed
        updateStockTable();
    }//GEN-LAST:event_ShowAllStockButtonActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        dbh.deleteProduct(Integer.parseInt(SDProdID.getText()));
        updateStockTable();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        String boxchoice = (String) jComboBox1.getSelectedItem();
        dbh.modifyProduct(Integer.parseInt(SDProdID.getText()), Integer.parseInt(ModField.getText()), boxchoice);
        updateStockTable();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void SCRemoveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SCRemoveButtonActionPerformed
        updateShoppingCart();
        Predicate<Product> productPredicate = p-> p.getId() == Integer.parseInt(ProductIDField.getText());
        cart.removeIf(productPredicate);
        updateShoppingCart();
    }//GEN-LAST:event_SCRemoveButtonActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        
        dbh.addStock(SAName.getText(), Integer.parseInt(SAStock.getText()), Integer.parseInt(SAPrice.getText()), SATag1.getText(), SATag2.getText(), SATag3.getText());
        updateStockTable();
    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox AdminCheckBox;
    private javax.swing.JPanel BottomPane;
    private javax.swing.JButton CAddButton1;
    private javax.swing.JTextField CEmail;
    private javax.swing.JTextField CName1;
    private javax.swing.JLabel CNameText1;
    private javax.swing.JLabel CRegisterText1;
    private javax.swing.JButton CRemoveButton1;
    private javax.swing.JLabel CRemoveText1;
    private javax.swing.JTextField CSurname1;
    private javax.swing.JLabel CSurnameText1;
    private javax.swing.JTextField CUserIDText1;
    private javax.swing.JLabel CUserIdText1;
    private javax.swing.JLabel CUsernameText1;
    private javax.swing.JPanel CustomerAccounts;
    private javax.swing.JScrollPane CustomerResultScroll;
    private javax.swing.JTable CustomerResultTable;
    private javax.swing.JLabel LogText;
    private javax.swing.JLabel LoggedUser;
    private javax.swing.JButton LoginButton;
    private javax.swing.JTextField ModField;
    private javax.swing.JPasswordField Password;
    private javax.swing.JTextField ProductIDField;
    private javax.swing.JTextField ProductQuantityField;
    private javax.swing.JTextArea ReceiptPane;
    private javax.swing.JTextField SAName;
    private javax.swing.JTextField SAPrice;
    private javax.swing.JTextField SAStock;
    private javax.swing.JTextField SATag1;
    private javax.swing.JTextField SATag2;
    private javax.swing.JTextField SATag3;
    private javax.swing.JButton SCAddButton;
    private javax.swing.JTextField SCCustomerID;
    private javax.swing.JButton SCGenButton;
    private javax.swing.JLabel SCLabel;
    private javax.swing.JButton SCRemoveButton;
    private javax.swing.JTextField SDProdID;
    private javax.swing.JPanel Sales;
    private javax.swing.JTextField SalesIDPast;
    private javax.swing.JButton SalesSearchButton;
    private javax.swing.JButton SearchButtonStock;
    private javax.swing.JTextField SearchField;
    private javax.swing.JComboBox<String> SearchParameter;
    private javax.swing.JLabel SearchText;
    private javax.swing.JPanel ShoppingCart;
    private javax.swing.JButton ShowAllStockButton;
    private javax.swing.JPanel Stock;
    private javax.swing.JPanel StockAdminPanel;
    private javax.swing.JTabbedPane Tabs;
    private javax.swing.JComboBox<String> TagChoice;
    private javax.swing.JLabel TagText;
    private javax.swing.JButton UAAddButton;
    private javax.swing.JTextField UAName;
    private javax.swing.JLabel UANameText;
    private javax.swing.JTextField UAPassword;
    private javax.swing.JLabel UAPasswordText;
    private javax.swing.JLabel UARegisterText;
    private javax.swing.JButton UARemoveButton;
    private javax.swing.JLabel UARemoveText;
    private javax.swing.JTextField UASurname;
    private javax.swing.JLabel UASurnameText;
    private javax.swing.JLabel UAUserIDLabel;
    private javax.swing.JTextField UAUserIDText;
    private javax.swing.JTextField UAUsername;
    private javax.swing.JLabel UAUsernameText;
    private javax.swing.JPanel UserAccounts;
    private javax.swing.JTable UserResultTable;
    public javax.swing.JTextField Username;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSpinner jSpinner1;
    private javax.swing.JSpinner jSpinner2;
    private javax.swing.JSpinner jSpinner3;
    private javax.swing.JSpinner jSpinner4;
    private javax.swing.JSpinner jSpinner5;
    private javax.swing.JSpinner jSpinner6;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable3;
    private javax.swing.JTable jTable5;
    // End of variables declaration//GEN-END:variables
}
