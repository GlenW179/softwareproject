/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Eman
 */
public class Product {
    String productName, tag1, tag2, tag3;
    float price;
    int id, stockAmt;
    public Product(String productName, int stockAmt, float price, String tag1, String tag2, String tag3) {
        this.productName = productName;
        this.stockAmt = stockAmt;
        this.price = price;
        this.tag1 = tag1;
        this.tag2 = tag2;
        this.tag3 = tag3;
    }
    
    public Product(int id, String productName, int stockAmt, float price, String tag1, String tag2, String tag3) {
        this.productName = productName;
        this.stockAmt = stockAmt;
        this.price = price;
        this.tag1 = tag1;
        this.tag2 = tag2;
        this.tag3 = tag3;
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public String getProductName() {
        return productName;
    }
    
    public int getStockAmt() {
        return stockAmt;
    }
    
    public float getPrice() {
        return price;
    }
    
    public String getTag1() {
        return tag1;
    }
    
    public String getTag2() {
        return tag2;
    }
    
    public String getTag3() {
        return tag3;
    }
}
