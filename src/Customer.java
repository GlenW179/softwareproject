/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Eman
 */
public class Customer {
    public String name, surname, email;
    public int id;
    
    Customer(String name, String surname, String email) {
        this.name = name;
        this.surname = surname;
        this.email = email;
    }
    
    Customer(int id, String name, String surname, String email) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public String getEmail() {
        return email;
    }
    public int getId() {
        return id;
    }
}
